import React from "react";

class Tabel extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            data: this.props.anggota,
            warna: true,
            'number': 0,
        }
    }

    onClickBtn(){
        let { number, warna } = this.state;
        number += 1; 
        this.setState({ warna:!warna, number });
    }

    
    // componentDidMount(){
    //     console.log(this.props.anggota)
    // }

    render (){
        const { title } = this.props;
        const { anggota, warna, data, number } = this.state;
        return (<span>
            <button className="btn btn-info" onClick={() => this.onClickBtn()}>Tombol</button>
            <div>
                <h3 style={{
                    'color': warna ? 'white' : 'red'
                }}>{ title }</h3>

                <h2>{ number }</h2>
                <ol>
                    { data.length ? data.map((item, index) => (
                        <li key={index}>{ item.nama }</li>
                    ))
                        :
                        <li>Data tidak ada</li>
                    }
                </ol>
            </div></span>
        )
    }
}

export default Tabel

