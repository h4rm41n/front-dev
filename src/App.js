import logo from './logo.svg';
import './App.css';
import Tabel from './komponen/Tabel';


const App = () => {
  let anggota = [
    // {
    //   'nama': 'Andi1',
    //   'nim': '12345',
    // },
    // {
    //   'nama': 'Andi2',
    //   'nim': '12346',
    // },
    // {
    //   'nama': 'Andi3',
    //   'nim': '12347',
    // }
  ]
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Tabel anggota={anggota} title="Data Anggota Perpus STMIK" />
      </header>
    </div>
  );
}

export default App;
